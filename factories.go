package edispatcher_pg

import (
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"database/sql"
	"fmt"
)

// NewEventsStore реализует фабрику Events Store
func NewEventsStore(
	db *sql.DB,
	eventsTable string,
	eventsBatchSize uint8,
) edispatcher.EventsStoreInterface[*sql.Tx] {
	_, _ = db.Exec(fmt.Sprintf(`
        create table if not exists %v
        (
            id serial not null constraint %v_pk primary key,
            data json not null
        );
    `, eventsTable, eventsTable))

	return &eventsStore{
		db:              newDbClient(db),
		eventsTable:     eventsTable,
		eventsBatchSize: eventsBatchSize,
	}
}

// NewTransactionManager реализует фабрику менеджера транзакций
func NewTransactionManager(db *sql.DB) edispatcher.TransactionManagerInterface[*sql.Tx] {
	return &transactionManager{
		db: newDbClient(db),
	}
}

// NewEventsReceiveStore реализует фабрику сервиса для работы с полученными событиями
func NewEventsReceiveStore(
	db *sql.DB,
	eventsReceiveTable string,
) edispatcher.EventsReceiveStoreInterface[*sql.Tx] {
	_, err := db.Query(fmt.Sprintf(`select * from %v`, eventsReceiveTable))
	if nil != err {
		_, _ = db.Exec(fmt.Sprintf(`
			create table if not exists %v
			(
				id serial not null constraint %v_pk primary key,
				last_event_offset text not null
			);
		`, eventsReceiveTable, eventsReceiveTable))

		_, _ = db.Exec(fmt.Sprintf(`
			insert into %v(last_event_offset) values ('-1')
		`, eventsReceiveTable))
	}

	return &eventsReceiveStore{
		db:                 newDbClient(db),
		eventsReceiveTable: eventsReceiveTable,
	}
}

// NewPgEventsDispatcher реализует фабрику сервиса доставки событий с
// PostgresSQL в качестве хранилища данных событий
func NewPgEventsDispatcher(
	channel edispatcher.EventChannelInterface,
	busManager edispatcher.BusEventManagerInterface,
	db *sql.DB,
	eventsTablePrefix string,
	eventsBatchSize uint8,
	channelPull int,
) edispatcher.EventsDispatcherInterface[*sql.Tx] {
	eventsReceiveTable := fmt.Sprintf(`%v_receive`, eventsTablePrefix)

	return edispatcher.NewEventsDispatcher(
		channel,
		NewEventsStore(db, eventsTablePrefix, eventsBatchSize),
		NewEventsReceiveStore(db, eventsReceiveTable),
		NewTransactionManager(db),
		busManager,
		channelPull,
	)
}

// NewPgEventsDispatcherWithTimeout реализует фабрику сервиса доставки событий с PostgresSQL в качестве
// хранилища данных событий с настраиваемым таймаутом
func NewPgEventsDispatcherWithTimeout(
	channel edispatcher.EventChannelInterface,
	busManager edispatcher.BusEventManagerInterface,
	db *sql.DB,
	eventsTablePrefix string,
	eventsBatchSize uint8,
	channelPull int,
	timeoutMs uint16,
) edispatcher.EventsDispatcherInterface[*sql.Tx] {
	eventsReceiveTable := fmt.Sprintf(`%v_receive`, eventsTablePrefix)

	return edispatcher.NewEventsDispatcherWithTimeout(
		channel,
		NewEventsStore(db, eventsTablePrefix, eventsBatchSize),
		NewEventsReceiveStore(db, eventsReceiveTable),
		NewTransactionManager(db),
		busManager,
		channelPull,
		timeoutMs,
	)
}
