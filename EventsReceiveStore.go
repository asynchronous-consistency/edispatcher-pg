package edispatcher_pg

import (
	"bitbucket.org/go-generics/sqd"
	"context"
	"database/sql"
	"fmt"
	"github.com/pkg/errors"
)

// Сервис работы с полученными событиями
type eventsReceiveStore struct {
	db                 *dbClient
	eventsReceiveTable string
}

// GetLastReceivedEventId возвращает ID последнего принятого события
func (e eventsReceiveStore) GetLastReceivedEventId(ctx context.Context, tx *sql.Tx) (id string, err error) {
	return sqd.HandleWithTx[eventsReceiveStore, string, *sql.Tx, *sql.TxOptions](
		e.db,
		ctx,
		tx,
		func(ctx context.Context, tx *sql.Tx) (string, error) {
			rows, err := tx.QueryContext(ctx, fmt.Sprintf(
				`select last_event_offset from %v where id = 1`,
				e.eventsReceiveTable,
			))

			if nil != err {
				return ``, errors.Wrap(err, `failed to load last received event ID`)
			}

			id, err := sqd.Scan[string](rows, sqd.IdScan[string])
			if nil != err {
				return ``, errors.Wrap(err, `failed to scan last received event ID`)
			}

			if 0 == len(id) {
				return ``, errors.New(`last received event ID is not set in database`)
			}

			return id[0], nil
		},
	)
}

// SetLastReceivedEventId устанавливает ID последнего принятого события
func (e eventsReceiveStore) SetLastReceivedEventId(ctx context.Context, eventId string, tx *sql.Tx) (err error) {
	_, err = sqd.HandleWithTx[eventsReceiveStore, any, *sql.Tx, *sql.TxOptions](
		e.db,
		ctx,
		tx,
		func(ctx context.Context, tx *sql.Tx) (any, error) {
			_, err := tx.ExecContext(
				ctx,
				fmt.Sprintf(
					`update %v set last_event_offset=$1 where id=1`,
					e.eventsReceiveTable,
				),
				eventId,
			)

			if nil != err {
				return ``, errors.Wrap(err, `failed to set last received event ID`)
			}

			return nil, nil
		},
	)

	return
}
