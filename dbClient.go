package edispatcher_pg

import (
	"context"
	"database/sql"
)

// dbClient реализует обертку над стандартным клиентом sql.DB для библиотеки sqd.
type dbClient struct {
	MainClient *sql.DB
}

// StartSqdTransaction выполняет инициализацию транзакции и возвращает ее
func (t dbClient) StartSqdTransaction(ctx context.Context, options *sql.TxOptions) (*sql.Tx, error) {
	return t.MainClient.BeginTx(ctx, options)
}

// newDbClient реализует конструктор клиента
func newDbClient(client *sql.DB) *dbClient {
	return &dbClient{
		MainClient: client,
	}
}
