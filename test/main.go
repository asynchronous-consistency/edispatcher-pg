package main

import (
	"bitbucket.org/asynchronous-consistency/edispatcher-pg/v3"
	"context"
	"database/sql"
	"encoding/json"
	log "github.com/sirupsen/logrus"
	prefixed "github.com/x-cray/logrus-prefixed-formatter"
)

func main() {
	log.SetLevel(log.InfoLevel)
	log.SetFormatter(new(prefixed.TextFormatter))

	connStr := "host=localhost port=5432 user=test password=test dbname=test sslmode=disable sslrootcert= search_path=\"public\""
	db, err := sql.Open("postgres", connStr)

	if nil != err {
		log.Fatal(err.Error())
		return
	}

	txManager := edispatcher_pg.NewTransactionManager(db)

	eventsStore := edispatcher_pg.NewEventsStore(db, "evts", 50)

	err = eventsStore.CreateEvents(context.Background(), []string{`{"test": 1, "data": "te'st"}`, "2"}, nil)
	if nil != err {
		log.Fatal(err.Error())
	}

	tx, err := txManager.Begin(nil)
	if nil != err {
		log.Fatal(err.Error())
	}

	eventsStore.CreateEvents(context.Background(), []string{"3", "4"}, tx)
	txManager.Rollback(context.Background(), tx)

	tx, _ = txManager.Begin(nil)
	eventsStore.CreateEvents(context.Background(), []string{"5", "6"}, tx)
	txManager.Commit(context.Background(), tx)

	ev, _ := eventsStore.LoadEvents(nil, nil)
	data, _ := json.Marshal(ev)

	log.Println(string(data))

	eventsReceive := edispatcher_pg.NewEventsReceiveStore(db, "evtss")
	id, err := eventsReceive.GetLastReceivedEventId(nil, nil)
	if nil != err {
		log.Fatal(err.Error())
	}

	log.Println(id)

	eventsReceive.SetLastReceivedEventId(nil, "22", nil)
	id, err = eventsReceive.GetLastReceivedEventId(nil, nil)
	if nil != err {
		log.Fatal(err.Error())
	}

	log.Println(id)

	eventsReceive.SetLastReceivedEventId(nil, "44", nil)
	id, err = eventsReceive.GetLastReceivedEventId(nil, nil)
	if nil != err {
		log.Fatal(err.Error())
	}

	log.Println(id)
}
