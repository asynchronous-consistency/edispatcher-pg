package edispatcher_pg

import (
	"context"
	"database/sql"
)

// Менеджер транзакций
type transactionManager struct {
	db *dbClient
}

// Begin выполняет запуск транзакции
func (t transactionManager) Begin(ctx context.Context) (*sql.Tx, error) {
	if nil == ctx {
		ctx = context.Background()
	}

	return t.db.MainClient.BeginTx(ctx, nil)
}

// Rollback выполняет откат изменений
func (t transactionManager) Rollback(_ context.Context, tx *sql.Tx) {
	tx.Rollback()
}

// Commit выполняет коммит изменений
func (t transactionManager) Commit(_ context.Context, tx *sql.Tx) {
	tx.Commit()
}
