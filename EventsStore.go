package edispatcher_pg

import (
	. "bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"bitbucket.org/go-generics/sqd"
	"context"
	"database/sql"
	"fmt"
	"github.com/lib/pq"
	"github.com/pkg/errors"
	"strings"
)

// eventsStore реализует сервис работы с событиями на стороне сервера,
// использующего библиотеку. Использует в качестве БД PostgresSQL.
type eventsStore struct {
	db              *dbClient
	eventsTable     string
	eventsBatchSize uint8
}

// CreateEvents выполняет создание событий для доставки в шину
func (e eventsStore) CreateEvents(ctx context.Context, data []string, tx *sql.Tx) (err error) {
	if 0 == len(data) {
		return
	}

	_, err = sqd.HandleWithTx[eventsReceiveStore, any, *sql.Tx, *sql.TxOptions](
		e.db,
		ctx,
		tx,
		func(ctx context.Context, tx *sql.Tx) (any, error) {
			values := make([]string, 0, len(data))
			dataToInsert := make([]any, 0, len(data))

			for i, val := range data {
				values = append(values, fmt.Sprintf(`($%v)`, i+1))
				dataToInsert = append(dataToInsert, val)
			}

			_, err = tx.ExecContext(
				ctx,
				fmt.Sprintf(`insert into %v(data) values %v`, e.eventsTable, strings.Join(values, ", ")),
				dataToInsert...,
			)

			if nil != err {
				return nil, errors.Wrap(err, `failed to insert new events data to DB`)
			}

			return nil, nil
		},
	)

	return
}

// LoadEvents загружает события, сохраненные для доставки в шину
func (e eventsStore) LoadEvents(ctx context.Context, tx *sql.Tx) (res []*StoredEventData, err error) {
	return sqd.HandleWithTx[eventsReceiveStore, []*StoredEventData, *sql.Tx, *sql.TxOptions](
		e.db,
		ctx,
		tx,
		func(ctx context.Context, tx *sql.Tx) ([]*StoredEventData, error) {
			rows, err := tx.Query(fmt.Sprintf(
				`select id, data from %v order by id limit %v`,
				e.eventsTable,
				e.eventsBatchSize,
			))

			if nil != err {
				return nil, errors.Wrap(err, `failed to load base events data`)
			}

			events, err := sqd.Scan[*StoredEventData](
				rows,
				func(initialItem *StoredEventData, scan sqd.TScan) (*StoredEventData, error) {
					var result StoredEventData
					err := scan(&result.Id, &result.Data)
					if nil != err {
						return nil, err
					}

					return &result, nil
				},
			)

			if nil != err {
				return nil, errors.Wrap(err, `failed to scan events data`)
			}

			return events, nil
		},
	)
}

// DeleteEvents удаляет события, отправленные в шину
func (e eventsStore) DeleteEvents(ctx context.Context, eventIds []string, tx *sql.Tx) (err error) {
	if 0 == len(eventIds) {
		return
	}

	_, err = sqd.HandleWithTx[eventsReceiveStore, any, *sql.Tx, *sql.TxOptions](
		e.db,
		ctx,
		tx,
		func(ctx context.Context, tx *sql.Tx) (any, error) {
			_, err = tx.ExecContext(
				ctx,
				fmt.Sprintf(`delete from %v where id = any($1)`, e.eventsTable),
				pq.Array(eventIds),
			)

			if nil != err {
				return nil, errors.Wrap(err, `failed to delete old events data from DB`)
			}

			return nil, nil
		},
	)

	return
}
